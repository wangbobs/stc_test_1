/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_led.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-21   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_led.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/        
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
/**
  * @brief Initialize LED
  * @bug none
  */
void led_init()
{
    P2 &=(uint8_t)~(1<<0);
    P11=0;
    P0=0xff;
}
/**
  * @brief Light up LED
  * @param[in] pd: LED data
  * @param[in] pd: LED STATUS
  */
void led_switch(LED_e ledn,LED_status status)
{
    if(ledn<LED_MAX)
    {
        if (status==0)
           P0&=~(1<<ledn);
        else
           P0|=(1<<ledn);
    }
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
