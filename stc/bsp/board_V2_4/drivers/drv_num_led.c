/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_num_led.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-22   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_num_led.h"
#include <STC12C5A60S2.H>

/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/        
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
code uint8_t NUM_ARRAY[] = {0xC0, 0XF9, 0XA4, 0XB0, 0X99, 0x92, 0X82, 0XF8, 0X80, 0X90,0xff};
volatile uint8_t NUM_Buff[7];
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
/**
  * @brief Initialize NUM_LED
  * @bug 
  */
void num_led_init()
{
    uint8_t i;
    P11=0;
    P0=0xff;
    for(i=0;i<7;i++)
    {
        NUM_Buff[i]=0xff;
    }
    
}
/**
  * @brief Light up LED
  * @param[in] pd: LED data
  * @param[in] pd: LED STATUS
  */

void num_led_show(uint32_t num)
{
    uint8_t n=6;
    P11=1;
    P2 &= ~(7<<0);
    while(n--)
    {
        P2 |= (n % 7<<0);
        P0=NUM_ARRAY[num%10];
        num /=10;
    }
    P11=0;
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/
