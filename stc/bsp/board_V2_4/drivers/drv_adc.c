/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_adc.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning standard input and output library functions are not recommended. (such as printf and scanf fuctions in <stdio.h>)
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-14   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_adc.h"
#include<intrins.h>
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
#define ADC_POWER 0x80 
#define ADC_FLAG 0x10 
#define ADC_START 0x08 
#define ADC_SPEEDLL 0x00 //540 clocks
#define ADC_SPEEDL 0x20 //360 clocks
#define ADC_SPEEDH 0x40 //180 clocks
#define ADC_SPEEDHH 0x60
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
static void Delay5ms();
static uint8_t Get_ADCResult(uint8_t ch);
/**
  * @brief initialize ADC
  * @bug no timeout mechanism, may fall into an infinite loop
  */
void adc_init()
{
    P1ASF |=0X01;
    ADC_RES = 0;
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL;
    Delay5ms();
    
}
static uint8_t Get_ADCResult(uint8_t ch)
{
    ADC_CONTR = ADC_POWER|ADC_SPEEDLL|ADC_START|ch;   /* ADRJ = 0; 为8位ADC转换器 */
    Delay5ms();
	while(!(ADC_CONTR&ADC_FLAG));    /* 当ADC_FLAG为1时表示完成 */
	ADC_CONTR &= ~ADC_FLAG;
	return ADC_RES;
}
float Get_Voltage(uint8_t ch)
{
    uint8_t vol;
	float value;
	vol = Get_ADCResult(ch);
	value = 5 * ((float)vol / 256);    /* 注意数据类型 */
	return value;
}

static void Delay5ms()		//@33.1776MHz
{
	unsigned char i, j, k;

	_nop_();
	_nop_();
	i = 1;
	j = 162;
	k = 89;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}




/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/

